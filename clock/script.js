
setInterval(() => {
    setDate('.containerBr')
    setDate('.containerCh')
    setDate('.containerCn')
}, 1000)

function setDate(selector){
    const relogio = document.querySelector(selector)
    relogio.atribute('hora');
    const hour = relogio.querySelector('[data-hour-hand]');
    const min = relogio.querySelector('[data-minute-hand]');
    const sec = relogio.querySelector('[data-second-hand]');


    const currentDate = new Date();
    const seconds = currentDate.getSeconds() / 60;
    const minutes = (seconds + currentDate.getMinutes()) / 60;
    const hours = (minutes + currentDate.getHours()) / 12;

    setRotation(sec, seconds);
    setRotation(min, minutes);
    setRotation(hour, hours);
}

function setRotation(element, rotationRatio){
    element.style.setProperty('--rotation', rotationRatio * 360);
}

setDate('.containerBr')