import tkinter as tk
from tk import *
 
class minhaApp_tk(tk.Tk):
                                                                        # Tkinter.tk é a classe base para a janela padrão. A nossa classe minhaApp_tk irá herdar todas as funcionalidades da classe padrão.
 
    def __init__(self,parent):
        tk.Tk.__init__(self,parent)
                                                                        # no construtor da nossa classe, apenas chamamo o construtor da classe pai, Tkinter.Tk.__init__()).
        self.parent = parent
                                                                        # geralmente necessitaremos  de acessar o pai de um objeto. É uma boa técnica  sempre salvar uma referencia ao pai.
        self.initialize()
                                                                        #no método initialize criamos os demais objetos que serão apresentados na tela, inicializamos as variáveis globais (irc..),
                                                                        #inicializamos o hardware caso necessário, etc..
         
         
    def initialize(self):                                                #aqui criamos os widgets que serão apresentados na tela
         
        #criação do menu principal
        topo = self.winfo_toplevel()                                     #pegamos aqui a referencia à janela principal
        self.menuBar = tk.Menu(topo)                                     #Criamos uma barra de menus nesta janlea
         
        mnuArquivo = tk.Menu(self.menuBar, tearoff=0)                       #criamos o menu arquivo
        mnuArquivo.add_command(label="Novo", command=self.processaTBD)      #criamos diferentes opções. Apenas como exemplo
        mnuArquivo.add_command(label="Salvar", command=self.processaTBD)    #mantivemos o padrão Novo/Salvar/Sair
        mnuArquivo.add_separator()                                          #uma barra separa o comando de sair do software
        mnuArquivo.add_command(label="Sair", command=topo.quit)             #ao comando sair associamos o método que fecha o programa
        self.menuBar.add_cascade(label="Arquivo", menu=mnuArquivo)          #associamos estes submenus ao menu arquivo
 
                                                                             # criamos mais alguns menus  apenas como exemplo
        mnuEditar = tk.Menu(self.menuBar, tearoff=0)
        mnuEditar.add_command(label="Cortar", command=self.processaTBD)      #associamos vários menus ao método processaTBD
        mnuEditar.add_command(label="Copiar", command=self.processaTBD)      #este método não faz nada, apenas proporciona
        mnuEditar.add_command(label="Colar", command=self.processaTBD)       #o caminho completo para o processamento do evento gerado
        self.menuBar.add_cascade(label="Editar", menu=mnuEditar)             #ao ser clicado o meni associado.
 
        mnuAjuda = tk.Menu(self.menuBar, tearoff=0)
        mnuAjuda.add_command(label="About", command=self.processaTBD)
        self.menuBar.add_cascade(label="Help", menu=mnuAjuda)
 
        # display the menu
        topo.config(menu=self.menuBar)

    def processaTBD(self):
        pass
     
    def processaSobre(self):
        pass
         
         
    def processaSair(self):
        pass       
     
                                    #este é ponto onde o programa se inicia
                                    #se o programa foi chamado a partir do interpretador python, o _name_  automaticamente será "__main__"
 
if __name__ == "__main__":
    app = minhaApp_tk(None)         #criamos uma aplicação sem nenhum pai, pois é a principal.
    app.title('Teste')              #especificamos o título de nossa aplicação
    app.mainloop()                  #o programa entra no loop de espera de eventos (pressionar de menus, botões, etc..)
    