import _tkinter as Tk
from logging import root
import re
import math
import tkinter as tk
from typing import List
from tk import *

# God class - classe que faz tudo, permite manipular display, label, etc.. ##


class Calculator(tk.Tk):

    def __init__(
        self,
        root: tk.Tk,
        label: tk.Label,
        display: tk.Entry,
        buttons: List[List[tk.Button]]


    ):
        self.root = root
        self.label = label
        self.display = display
        self.buttons = buttons
     

    def start(self):
        
        self._limpar_esc()
        self._config_buttons()
        self._config_display()
        self.root.mainloop()


    def _config_buttons(self):
        buttons = self.buttons
        for row_values in buttons:
            for button in row_values:
                button_text = button['text']

                if button_text == 'AC':
                    button.grid(columnspan=2)
                    button.bind('<Button-1>', self.clear)
                    button.config(background='#827c76',
                                  activebackground='#3e4543',
                                  highlightbackground='#3e4543'
                                  )

                if button_text == '⌫':
                    button.grid(column=2)
                    button.bind('<BackSpace>', self.backspace)
                    button.config(background='#827c76',
                                  activebackground='#3e4543',
                                  highlightbackground='#3e4543'
                                  )

                if button_text in '/' '+' '-' '*':
                    button.grid(column=3)
                    button.config(background='#9c551f',
                                  activebackground='#3e4543',
                                  highlightbackground='#3e4543'
                                  )

                if button_text in '0':
                    button.grid(columnspan=2)

                if button_text in '.':
                    button.grid(column=2)

                if button_text in '=':
                    button.grid(column=3)
                    button.config(background='#9c551f',
                                  activebackground='#3e4543',
                                  highlightbackground='#3e4543'
                                  )

                if button_text in '0123456789.+-/*%':
                    button.bind('<Button-1>', self.add_text_to_display)

                if button_text in '=':
                    button.bind('<Button-1>', self.calculate)


    def _limpar_esc(self):
        self.display.bind(
            '<Escape>', self.clear
        )

    def _config_display(self):
        self.display.bind(
            '<Return>', self.calculate
        )
        self.display.bind(
            '<KP_Enter>', self.calculate
        )
        self.display.bind(
            '<BackSpace>', self.clear('end')
        )

    def _fix_text(self, text):
        # Substitui o que não é 0123456789,/*-+%
        text = re.sub(r'[^\d\.\/\*\-\+\%e]', r'', text, 0)
        # Substitui sinais repetidos para apenas um sinal
        text = re.sub(r'([\.\+\/\-\*\%])\1+', r'\1', text, 0)
        # Substitui * para nada
        #text = re.sub(r'\*?', '', text)

        return text

    def clear(self, event=None):
        self.display.delete(0, 'end')

    def backspace(self, event=None):
        self.display.delete['end', -1]

    def add_text_to_display(self, event=None):
        self.display.insert('end', event.widget['text'])

    def state(self, event=None):
        self.display.insert('end', state='disabled')

    def calculate(self, event=None):
        fixed_text = self._fix_text(self.display.get())
        equations = self._get_equations(fixed_text)

        try:
            if len(equations) == 1:
                result = eval(self._fix_text(equations[0]))
            else:
                result = eval(self._fix_text(equations[0]))
                for equation in equations[1:]:
                    result = math.pow(result, eval(self._fix_text(equations)))

            self.display.delete(0, 'end')
            self.display.insert('end', result)
            self.label.config(text=f'{fixed_text} = {result}')

        except OverflowError:
            self.label.config(
                text='Não consegui realizar essa conta, desculpe!')
        except Exception as e:
            print(e)
            self.label.config(text='Conta inválida')

    def _get_equations(self, text):
        return re.split(r'\^', text, 0)
