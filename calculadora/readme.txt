Este projeto tem por finalidade o aprendizado e aperfeiçoamento das técnicas e práticas de programação na linguagem de Python e JavaScript.

##############################

* Calculadora básica que possui os seguintes recursos:

- Adição
- Subtração
- Multiplicação
- Divisão

##############################

* Durante o desenvolvimento da calculadora foram consultadas diversas fontes de conhecimento, como vídeos, artigos e etc.

##############################

* Ferramentas utilizadas durante o processo de aprendizagem e desenvolvimento:

- Python v3
- Git
- Visual Studio Code
- OS Linux Mint / Windows 10
- Framework Tkinter
- PIP
- Fish