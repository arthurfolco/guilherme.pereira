from logging import root
import tkinter as tk
from typing import List
from tk import *
from tkinter import ttk




def make_root() -> tk.Tk:
    root = tk.Tk()
    root.iconbitmap('assets\calculator.ico')
    root.title('Calculadora')
    # Configuração do padding ###
    root.config(padx=5, pady=5, background='#211f1f')
    root.resizable(False, False)  # Redimensionamento falso ###
    root.geometry()
    return root


def make_label(root) -> tk.Label:
    label = tk.Label(
        root, font='Futura', text='Nenhuma operação detectada',
        anchor='e', justify='right', background='#211f1f', fg='#ffffff'
    )
    label.grid(row=0, column=0, columnspan=4, sticky='news')
    return label


def make_display(root) -> tk.Entry:
    display = tk.Entry(root)
    display.grid(row=1, column=0, columnspan=4, sticky='news', pady=(0, 10)),
    display.focus_set()
    display.config(
        font=('Futura', 17, 'bold'),
        justify='right', bd=1, relief='flat', fg='#ffffff',
        highlightthickness=0, highlightcolor='#9c551f', background='#3e4543'
    )

    display.bind('<Control-a>', display_control_a)
    return display


def display_control_a(event):
    event.widget.select_range(0, 'end')
    event.widget.icursor('end')
    return 'break'


def make_buttons(root) -> List[List[tk.Button]]:
    button_texts: List[List[str]] = [

        ['AC', '⌫', '/'],
        ['7', '8', '9', '*'],
        ['4', '5', '6', '-'],
        ['1', '2', '3', '+'],
        ['0', '.', '=']
    ]

    buttons: List[List[tk.Button]] = []

    for row, row_value in enumerate(button_texts, start=2):
        button_row = []
        for col_index, col_value in enumerate(row_value):
            btn = tk.Button(root, text=col_value)
            btn.grid(row=row, column=col_index, sticky='news', padx=2, pady=2)
            btn.config(
                font=('Futura', 17, 'normal'),
                padx=10, pady=20, width=1, background='#3e4543', bd=0,
                cursor='hand2', highlightthickness=0, fg='#ffffff',
                highlightcolor='#ccc', activebackground='#9c551f',
                highlightbackground='#9c551f', relief='ridge'

            )

            button_row.append(btn)
        buttons.append(button_row)

    return buttons
